// (c) 2015 Vladimir Stadnik

// 7.2. Посчитать количество строк матрицы, сумма элементов которых есть
//      нечетное число, если матрица хранится в виде массива столбцов.
//      См. ф-цию malgorythm


#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

typedef struct _matrix_ {
    int size;
    int **data;
} Matrix;

char enter_number1[] = "Enter the string 1: ";
char enter_number2[] = "Enter the string 2: ";
char str_gsize[]     = "Enter size of matrix: ";
char str_size[]      = "%d";
char str_gindex[]    = "Enter indexes of element: ";
char str_index[]     = "%d %d";
char str_gelem[]     = "Enter element: ";
char str_elem[]      = "%d";
char str_elem_[]     = "%d ";
char str_endl[]      = "\n";

int** mcreate(int);          // Создать матрицу
void  mdestroy(Matrix *&);   // Уничтожить матрицу
int** mset(Matrix*);         // Поменять матрицу
int   mget(Matrix*);         // Получить элемент из матрицы
void  mchange(Matrix*);      // Изменить элемент а матрице
void  mprint(Matrix*);       // Вывести матрицу на поток
int   malgorythm(Matrix*);   // Алгоритм по заданию
void  mreplace(Matrix*);     // Переставить ?

char menu() {
    puts("Choose point of menu:");
    puts("1. Create matrix");
    puts("2. Destroy matrix");
    puts("3. Get element [i][j]");
    puts("4. Change element [i][j]");
    puts("5. Set matrix");
    puts("6. Print matrix");
    puts("7. Count string");
    puts("8. Replace ");
    puts("*. Exit");
    printf("%s", "Enter a key: ");

    char key;
    scanf("%c", &key);

    return key;
}

void SwitchMenu(char key, Matrix *&matrix) {
    if (!matrix) return;

    system("cls");

    switch(key) {

    case '1': {
        if (matrix && matrix->data)
            mdestroy(matrix);

        printf("%s", "Enter size: ");
        int n;
        scanf("%d", &n);

        matrix->data = mcreate(n);
        matrix->size = n;

        break;
    }

    case '2': {
        mdestroy(matrix);

        matrix->data = (int **) NULL;
        matrix->size = 0;

        break;
    }

    case '3': {
        printf("%d\n", mget(matrix));
        break;
    }

    case '4': {
        mchange(matrix);
        break;
    }

    case '5': {
        matrix->data = mset(matrix);
        break;
    }

    case '6': {
        mprint(matrix);
        break;
    }

    case '7': {
        printf("%d\n", malgorythm(matrix));
        break;
    }

    case '8': {
        mreplace(matrix);
        break;
    }

    case '*': {
        mdestroy(matrix);

        matrix->data = (int **) NULL;
        matrix->size = 0;

        break;
    }

    }
}



void mreplace(Matrix *matrix) {
    int j1, j2;

    __asm {
        lea     eax, enter_number1     // "Введите j1: "
        push    eax
        call    dword ptr printf
        add     esp, 4

        lea     eax, j1                // Ввели j1
        push    eax
        lea     eax, str_elem
        push    eax
        call    dword ptr scanf_s
        add     esp, 8

        lea     eax, enter_number2     // "Введите j2: "
        push    eax
        call    dword ptr printf
        add     esp, 4
        lea     eax, j2                // Ввели j2
        push    eax
        lea     eax, str_elem
        push    eax
        call    dword ptr scanf_s
        add     esp, 8

        mov     eax, matrix
        mov     ecx, [eax]             // размер
        mov     ebx, [eax+4]           // начало массива
        xor     edi, edi               // номер столбца 1
        xor     esi, esi               // номер столбца 2

        mov edi, j1
        mov esi, j2
        mov eax, [ebx][edi*4]
        mov ecx, [ebx][esi*4]

        mov [ebx][edi*4], ecx
        mov [ebx][esi*4], eax
    }
}


int** mcreate(int n)
{
    __asm {
        mov     ecx, n
        cmp     ecx, 0

        shl     ecx, 2
        push    ecx
        call    dword ptr malloc
        pop     ecx

        mov     edx, eax               // В еdx память под строку
        mov     ebx, ecx               // Размер для цикла

    CYC_MEM:
        sub     ecx, 4
        cmp     ecx, 0
        jl      GOOD

        push    ecx
        push    edx

        push    ebx
        call    dword ptr malloc
        pop     ebx

        pop     edx
        pop     ecx

        mov     dword ptr [edx][ecx], eax
        jmp     CYC_MEM

    GOOD:
        mov     eax, edx
        jmp     FIN

    FIN:
    }
}


void mdestroy(Matrix *&matrix) {
    __asm {
        mov     ebx, matrix
        mov     edx, [ebx]
        cmp     dword ptr [edx+4], 0
        je      FIN
        mov     ebx, [edx+4]
        mov     ecx, [edx]
        dec     ecx

    CYC_STR:
        push    ecx
        push    ebx
        mov     eax, [ebx][ecx*4]
        push    eax
        call    dword ptr free
        add     esp, 4
        pop     ebx
        pop     ecx

        dec     ecx
        cmp     ecx, 0
        jge     CYC_STR

    FIN_S:
        push    ebx
        call    dword ptr free
        add     esp, 4
        mov     eax, matrix
        mov     dword ptr [eax], 0

    FIN:
    }
}


int mget(Matrix *matrix) {
    int i, j;

    __asm {

    BEGIN:
        mov     eax, matrix            // Ук-ль на начало структуры
        lea     ebx, str_gindex
        push    ebx
        call    dword ptr printf
        add     esp, 4

        lea     ebx, j
        push    ebx
        lea     ebx, i
        push    ebx
        lea     ebx, str_index
        push    ebx
        call    dword ptr scanf_s
        add     esp, 12

        mov     esi, j
        mov     edi, i
        mov     edx, matrix

        mov     ebx, [edx+4]
        mov     ecx, [ebx][esi*4]
        mov     eax, [ecx][edi*4]
        jmp     FIN

        mov     eax, 0

    FIN:
    }
}


void mchange(Matrix *matrix) {
    int i, j;

    __asm {

    BEGIN:
        mov     eax, matrix             // На начало структуры

        lea     ebx, str_gindex
        push    ebx
        call    dword ptr printf
        add     esp, 4

        lea     ebx, j
        push    ebx
        lea     ebx, i
        push    ebx
        lea     ebx, str_index
        push    ebx
        call    dword ptr scanf_s      // На что меняем
        add     esp, 12
        mov     esi, j
        mov     edi, i
        mov     edx, matrix

        push    edi
        push    esi

        lea     ebx, str_gelem
        push    ebx
        call    dword ptr printf
        add     esp, 4

        lea     ebx, i
        push    ebx
        lea     ebx, str_elem
        push    ebx
        call    dword ptr scanf_s
        add     esp, 8
        mov     ecx, i

        pop     esi
        pop     edi

        mov     eax, matrix
        mov     edx, [eax+4]
        mov     ebx, [edx][esi*4]
        mov     dword ptr [ebx][edi*4], ecx
        jmp     FIN

    FIN:
    }
}


int** mset(Matrix *matrix) {
    int c;

    __asm {
        mov     eax, matrix

        mov     ecx, [eax]
        mov     ebx, [eax+4]
        lea     edx, str_elem          // %d

        xor     esi, esi

    CYC_STR:
        xor     edi, edi

    CYC_EL:
        push    ecx                    // Сохраняем размер
        push    ebx                    // Адрес массива
        push    esi                    // Номер строки
        push    edi                    // Номер столбца

        lea     eax, c
        push    eax
        push    edx
        call    dword ptr scanf_s
        pop     edx
        add     esp, 4

        pop     edi
        pop     esi
        pop     ebx

        mov     ecx, c
        mov     eax, [ebx][edi*4]
        mov     dword ptr [eax][esi*4], ecx

        pop     ecx

        inc     edi
        cmp     edi, ecx
        jl      CYC_EL

        inc     esi
        cmp     esi, ecx
        jl      CYC_STR

        mov     eax, ebx
        jmp     FIN_S

    FIN_S:
    }
}


void mprint(Matrix *matrix) {
    __asm {
        mov     eax, matrix

        mov     ecx, [eax]
        mov     ebx, [eax+4]
        lea     edx, str_elem_
        xor     esi, esi

    CYC_STR:
        xor     edi, edi

    CYC_EL:
        push    ecx                    // Сохраняем размер
        push    ebx                    // Начало массива
        push    esi                    // Номер строки
        push    edi                    // Номер столбца

        mov     eax, [ebx][edi*4]
        mov     ebx, [eax][esi*4]
        push    ebx
        push    edx
        call    dword ptr printf
        pop     edx
        add     esp, 4

        pop     edi
        pop     esi
        pop     ebx
        pop     ecx

        inc     edi
        cmp     edi, ecx
        jl      CYC_EL

        push    ecx
        push    ebx
        push    esi
        push    edx

        lea     edx, str_endl
        push    edx
        call    dword ptr printf       // Перeвод строки
        add     esp, 4

        pop     edx                    // %d
        pop     esi
        pop     ebx                    // Начало массива
        pop     ecx

        inc     esi
        cmp     esi, ecx
        jl      CYC_STR

        jmp     FIN_S

    FIN_S:
    }
}


int malgorythm(Matrix *matrix) {
    __asm {
        mov     eax, matrix
        mov     ecx, [eax]             // Размер
        mov     ebx, [eax+4]           // Начало массива
        xor     eax, eax               // Счётчик рез-та
        xor     edi, edi               // Номер строки
        push    eax

    CYC_STR:
        xor     esi, esi               // Номер столбца
        xor     eax, eax               // Сумма строки

    CYC_EL:
        mov     edx, [ebx][esi*4]
        add     eax, [edx][edi*4]
        inc     esi
        cmp     esi, ecx
        jl      CYC_EL

        rcr     eax, 1                 // Проверка на нечётность
        jnc     FIN_C
        pop     eax
        inc     eax
        push    eax
        jmp     FIN_C

    FIN_C:
        inc     edi
        cmp     edi, ecx
        jl      CYC_STR
        jmp     FIN

    FIN:
        pop     eax
    }
}



int main(int argc, char **argv) {

    Matrix *matrix = (Matrix *) malloc( sizeof(Matrix) );
    matrix->data = (int **) NULL;

    char m;
    do {
        m = menu();
        SwitchMenu(m, matrix);
    } while (m != '*');

    free(matrix);

    return 0;
}

