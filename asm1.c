// (c) 2015 Vladimir Stadnik

// 7.1. Выделить те символы, которые встречаются в исходной строке указанное количество раз.

#include <stdio.h>

int main(int argc, char **argv) {

    char enter_string[] = "Enter the string: ";              // "Введите строку"
    char enter_number[] = "Enter the number of matches: ";   // "Введите число совпадений"
    char num_f[]        = "%d";                              // формат вывода числа

    const int  N = 1024;         // символов на данную строчку
          int  counts[256];      // массив кол-ва совпадений по букве
          int  number;           // сколько раз она вводится
          char str[N];           // строка, которая вводится

    __asm {
        // Сообщение в вводе строки
        lea     eax, enter_string
        push    eax
        call    dword ptr printf
        add     esp, 4

        // Непосредственный ввод строки
        mov     eax, N
        push    eax
        lea     eax, str
        push    eax
        call    dword ptr gets_s
        add     esp, 8

        // Сообщение о вводе кол-ва совпадений
        lea     eax, enter_number
        push    eax
        call    dword ptr printf
        add     esp, 4

        // Непосредственный ввод совпадений
        lea     eax, number
        push    eax
        lea     eax, num_f
        push    eax
        call    dword ptr scanf_s
        add     esp, 8

        // Обнуление массива счётчиков
        lea     eax, counts
        xor     esi, esi

    FOR:
        cmp     esi, 256
        jge     END
        mov     dword ptr [eax][esi*4], 0
        inc     esi
        jmp     FOR
        END:

        // Непосредственно, подсчёт совпадений
        xor     esi, esi
        lea     ebx, str
        lea     ecx, counts

    FOR_BEG_1:                 // Цикл по введённой строке
        xor     eax, eax;
        mov     al, [ebx]      // Смотрим на i-ый символ
        cmp     al, 0
        je      FOR_END_1
        cbw
        cwd
        rol     eax, 16
        mov     ax, dx
        rol     eax, 16        // Вычисляем индекс буквы в массиве
        mov     edi, eax
        mov     edx, [ecx][edi*4]
        inc     edx           // Элемент с этим индексом увеличиваем на 1
        mov     dword ptr [ecx][edi*4], edx
        inc     ebx
        jmp     FOR_BEG_1

    FOR_END_1:
        // Непосредственно, вывод массива счётчиков
        lea     eax, counts
        xor     esi, esi

    FOR_2:
        cmp     esi, 256
        je      END_2
        mov     ebx, number
        cmp     ebx, [eax][esi*4]    // Сравниваем кол-во совпадений с нужным номером
        jne     NEXT_2               // Если равно...
        push    esi
        push    eax
        push    esi
        call    dword ptr putchar    // ... то выводим её номер в виде символа с пом. putchar
        add     esp, 4
        pop     eax
        pop     esi

    NEXT_2:
        inc     esi
        jmp     FOR_2

    END_2:
    }

    return 0;
}

