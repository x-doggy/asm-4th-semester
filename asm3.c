// (c) 2015 Vladimir Stadnik

// 7.3
// Вычислить значение определенного интеграла функции на отрезке [a,b]
// указанным методом с шагом step. Чтобы вычислить значение функции, кроме
// многочленов, можно воспользоваться стандартной библиотекой.
// Решение оформить в виде функции

// double имя_метода(double a, double b, double (*pf)(double), double step = 1e-2);

// Метод  прямоугольников. В качестве примера, реализовать вычисление интеграла cos(x), x, exp(x).

#include <stdio.h>
#include <math.h>

inline void menu() {
    system("cls");

    char *menu_str   = "Which of functions to integrate?\n\n";
    char *enter_str  = "1. cos x\n2. x\n3. exp x\n4. Quit program\n\nEnter a number: ";
    char *str_fmt    = "%s%s";

    __asm {
        mov     eax, enter_str
        push    eax
        mov     eax, menu_str
        push    eax
        mov     eax, str_fmt
        push    eax
        call    dword ptr printf
        add     esp, 12
    }
}

double mycos(double x) {
    __asm {
        finit
        fld qword ptr x
        fcos
    }
}

double x(double x) {
    __asm {
        finit
        fld qword ptr x
    }
}

double myexp(double x) {
    return exp(x);
}


double integrateAsRect(double a, double b, double (*pf)(double), double step=1e-2) {
    double n1, res, x, sum = 0;
    int n, n2;

    __asm {
        finit

        fld     qword ptr b
        fsub    qword ptr a
        fdiv    qword ptr step
        fstp    qword ptr n1

        fstcw   word ptr n2
        or      word ptr n2, 0x0C00
        fistp   dword ptr n
        fldcw   word ptr n2
        fld     qword ptr n1
        fistp   dword ptr n

        mov     ecx, n

    CYCLE:
        // x_i-1 = a+(n-1)*step
        fld     qword ptr a
        fild    dword ptr n
        fld1
        fsubp   st(1), st(0)
        fld     qword ptr step
        fmulp   st(1), st(0)
        faddp   st(1), st(0)
        fld     qword ptr step
        fld1
        fld1
        faddp   st(1), st(0)
        fdivp   st(1), st(0)
        faddp   st(1), st(0)
        fstp    qword ptr x

        push    ecx
        fld     qword ptr x
        sub     esp, 8
        fstp    qword ptr [esp]
        call    dword ptr pf
        add     esp, 8
        pop     ecx

        fld     qword ptr sum
        faddp   st(1), st(0)
        fstp    qword ptr sum
        fstp    st(0)
        dec     n
        loop    CYCLE

        fld     qword ptr sum
        fld     qword ptr step
        fmul    st(0), st(1)
        fst     qword ptr res
    }
    return res;
}

int main(int argc, char **argv) {

    setlocale(LC_ALL, "rus");

    const double step = 0.01;
    char format_int[] = "%d";
    char format_str[] = "%s";
    char format_num[] = "%lf";
    char enter_a[] = "\n\nНеобходимо ввести границы интегрирования\nВведите a: ";
    char enter_b[] = "\nВведите b: ";
    char esc_res[] = "\nРезультат = ";
    double a, b, res;
    int choise = 0;

    __asm {
        finit

    BEGIN:
        call    dword ptr menu
        lea     eax, choise
        push    eax
        lea     eax, format_int
        push    eax
        call    dword ptr scanf
        add     esp, 12

        mov     edi, choise

        // выбор пункта меню
        cmp     edi, 1
        je      CASE1
        cmp     edi, 2
        je      CASE2
        cmp     edi, 3
        je      CASE3
        cmp     edi, 4
        je      CASE4
        jmp     BEGIN

    CASE1:
        // cos(x)
        lea     eax, enter_a
        push    eax
        lea     eax, format_str
        push    eax
        call    dword ptr printf
        add     esp, 8

        lea     eax, a
        push    eax
        lea     eax, format_num
        push    eax
        call    dword ptr scanf
        add     esp, 8

        lea     eax, enter_b
        push    eax
        lea     eax, format_str
        push    eax
        call    dword ptr printf
        add     esp, 8

        lea     eax, b
        push    eax
        lea     eax, format_num
        push    eax
        call    dword ptr scanf
        add     esp, 8

        fld     qword ptr step
        sub     esp, 8
        fstp    qword ptr [esp]
        lea     eax, mycos
        push    eax
        fld     qword ptr b
        sub     esp, 8
        fstp    qword ptr [esp]
        fld     qword ptr a
        sub     esp, 8
        fstp    qword ptr [esp]
        call    dword ptr integrateAsRect
        add     esp, 28
        jmp     NEXT

    CASE2:
        // x
        lea     eax, enter_a
        push    eax
        lea     eax, format_str
        push    eax
        call    dword ptr printf
        add     esp, 8

        lea     eax, a
        push    eax
        lea     eax, format_num
        push    eax
        call    dword ptr scanf
        add     esp, 8

        lea     eax, enter_b
        push    eax
        lea     eax, format_str
        push    eax
        call    dword ptr printf
        add     esp, 8

        lea     eax, b
        push    eax
        lea     eax, format_num
        push    eax
        call    dword ptr scanf
        add     esp, 8

        fld     qword ptr step
        sub     esp, 8
        fstp    qword ptr [esp]
        lea     eax, x
        push    eax
        fld     qword ptr b
        sub     esp, 8
        fstp    qword ptr [esp]
        fld     qword ptr a
        sub     esp, 8
        fstp    qword ptr [esp]
        call    dword ptr integrateAsRect
        add     esp, 28
        jmp     NEXT

    CASE3:
        // exp(x)
        lea     eax, enter_a
        push    eax
        lea     eax, format_str
        push    eax
        call    dword ptr printf
        add     esp, 8

        lea     eax, a
        push    eax
        lea     eax, format_num
        push    eax
        call    dword ptr scanf
        add     esp, 8

        lea     eax, enter_b
        push    eax
        lea     eax, format_str
        push    eax
        call    dword ptr printf
        add     esp, 8

        lea     eax, b
        push    eax
        lea     eax, format_num
        push    eax
        call    dword ptr scanf
        add     esp, 8

        fld     qword ptr step
        sub     esp, 8
        fstp    qword ptr [esp]
        lea     eax, myexp
        push    eax
        fld     qword ptr b
        sub     esp, 8
        fstp    qword ptr [esp]
        fld     qword ptr a
        sub     esp, 8
        fstp    qword ptr [esp]
        call    dword ptr integrateAsRect
        add     esp, 28
        jmp     NEXT

    CASE4:
        jmp     ENDPRG

    NEXT:
        fstp    qword ptr res
        lea     eax, esc_res
        push    eax
        lea     eax, format_str
        push    eax
        call    dword ptr printf
        add     esp, 8

        fld     qword ptr res
        sub     esp, 8
        fstp    qword ptr [esp]
        lea     eax, format_num
        push    eax
        call    dword ptr printf
        add     esp, 12

        jmp     BEGIN

    ENDPRG:
    }

    return 0;
}

